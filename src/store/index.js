import Vue from 'vue'

import Vuex from 'vuex'
Vue.use(Vuex)

import {
    getToken,
    setToken
} from '@/utils/token'

const store = new Vuex.Store({
    state: {
        tokenObj: getToken() || {}
    },

    mutations: {
        // 提供操作数据的方法
        saveToken(state, payload) {
            setToken(payload) 

            state.tokenObj = payload
        }
    }
})

export default store