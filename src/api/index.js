// 根据统一出口原则， 所有的文件暴露应该集中在一个文件里，
//  那样别人导入就方便了， 所以在api里再新建 `index.js`，
// 导入刚刚封装的函数， 并暴露出去
import {
    login
} from './login'

export const loginAPI = login

import {userInfo} from './user'
export const userInfoAPI = userInfo