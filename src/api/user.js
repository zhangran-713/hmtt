import request from '@/utils/request'



//封装一个获取用户资料的接口
export const userInfo = () => {
    return request({
        url: '/v1_0/user/profile',
    })
}