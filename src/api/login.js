import request from '../utils/request'

//封装一个请求登录的接口
export const login = (data) => {
    return request({
        url:'/v1_0/authorizations',
        method:'post',
        data
    })
}