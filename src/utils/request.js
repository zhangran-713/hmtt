import axios from 'axios'
import store from '@/store'
//设置基地址
const instance = axios.create({
    baseURL: 'http://toutiao.itheima.net/'
})

//给请求对象加请求拦截
// 所有通过instance发送的请求都会被拦截下来
instance.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    // 判断一下，有token才加
    if (store.state.tokenObj.token) {
        config.headers.Authorization = 'Bearer ' + store.state.tokenObj.token
    }
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});
//暴露出去
export default instance