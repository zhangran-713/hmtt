const token_key = 'hmtt-two'

//封装一个保存token的函数
export const setToken = function(tokenObj){
    window.localStorage.setItem(token_key, JSON.stringify(tokenObj))
}

//封装一个函数获取Token
export const getToken = function(){
    return JSON.parse(window.localStorage.getItem(token_key))
}

//封装一个用来删除token的方法
export const removeToken =() =>{ 
    window.localStorage.removeItem(token_key)
}