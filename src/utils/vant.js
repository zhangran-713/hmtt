import Vue from 'vue'
import { NavBar } from 'vant';
import { Form } from 'vant';
import { Field } from 'vant';
import { Button } from 'vant';
import { Toast } from 'vant';
import { Tabbar, TabbarItem } from 'vant';
import { Icon } from 'vant';
import { Image as VanImage } from 'vant';
import { Cell, CellGroup } from 'vant';


Vue.use(Toast);
Vue.use(Cell);
Vue.use(CellGroup);
import { Col, Row } from 'vant';

Vue.use(Col);
Vue.use(Row);


Vue.use(VanImage);
Vue.use(Icon);
Vue.use(Tabbar);
Vue.use(TabbarItem);


Vue.use(Button);
Vue.use(Form);
Vue.use(Field);
Vue.use(NavBar);