import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login'
import Layout from '../views/Layout'
Vue.use(VueRouter)

import Home from '../views/Layout/views/Home'
import Ask from '../views/Layout/views/Ask'
import Video from '../views/Layout/views/Video'
import User from '../views/Layout/views/User'
import store from '../store'


const routes = [

  {
    path: '/login',
    component: Login
  },
  {
    path: '/layout',
    component: Layout,
    children: [

      {
        path: 'home',
        component: Home
      },
      {
        path: 'ask',
        component: Ask
      },
      {
        path: 'video',
        component: Video
      },
      {
        path: 'user',
        component: User,
        meta: {
          needLogin: true
        } //加一个标签
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

//给router对象加导航守卫
router.beforeEach((to, from, next) => {
    if (to.meta.needLogin) {
      if (store.state.tokenObj.token) {
        next()
      } else {
        Vue.prototype.$toast.fail('请登录')
        next('/login')
        // next({
        //   name: 'login',
        //   query: {
        //     back: to.path //to.path   //to.path就是本来要去的页面
        //   }
      // })
  }
}
else {
  next()
}
})

export default router